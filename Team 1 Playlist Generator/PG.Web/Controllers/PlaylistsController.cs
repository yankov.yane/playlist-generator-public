﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PagedList;
using PG.Models;
using PG.Services.Contract;
using PG.Services.DTOs;
using PG.Web.Models;
using PG.Web.Models.Mappers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PG.Web.Controllers
{
    [Authorize]
    public class PlaylistsController : Controller
    {
        private readonly IPlaylistService _playlistService;
        private readonly IGenreService _genreService;
        private readonly IBingMapsAPIService _bingMapsAPIService;
        private readonly UserManager<User> _userManager;

        public PlaylistsController(IPlaylistService playlistService, IGenreService genreService, IBingMapsAPIService bingMapsAPIService,
            UserManager<User> userManager)
        {
            _playlistService = playlistService;
            _genreService = genreService;
            _bingMapsAPIService = bingMapsAPIService;
            _userManager = userManager;
        }

        [AllowAnonymous]
        public async Task<IActionResult> Index(string searchQuery = "", string genre = "", string duration = "")
        {
            IEnumerable<PlaylistDTO> playlistsDTOs = await _playlistService.GetAllPlaylistsWithSettings(searchQuery, genre, duration);
            IList<PlaylistViewModel> playlistsViewModels = playlistsDTOs.Select(x => x.ToViewModel()).ToList();

            var allGenres = await _genreService.GetAllGenres();

            var genresAndDefault = new List<string>();
            genresAndDefault.AddRange(allGenres.Select(x => x.Name));
            ViewBag.Genres = genresAndDefault;

            ViewBag.MaxPlaylistDuration = _playlistService.GetMaxPlaylistDuration(playlistsDTOs);

            return View(playlistsViewModels);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<int> Create(string startLocation, string endLocation, string playlistName, int metal, int rock, int pop, int chalga, bool topTracks, bool sameArtist)
        {

            int tripTime = await _bingMapsAPIService.FindDuration(startLocation, endLocation);
            var user = await _userManager.GetUserAsync(User);

            var playlistId = await _playlistService.GeneratePlaylist(tripTime, playlistName, metal, rock, pop, chalga, topTracks, sameArtist, user);

            return playlistId;
        }

        public async Task<IActionResult> Edit(int id)
        {
            var playlist = await _playlistService.GetPlaylistById(id);

            return View(playlist.ToViewModel());
        }

        [HttpPost]
        public async Task<IActionResult> Edit(string playlistId, string playlistTitle, string playlistPixabayImage)
        {
            var playlist = await _playlistService.Update(int.Parse(playlistId), playlistTitle, playlistPixabayImage);

            return View(playlist.ToViewModel());
        }


        [AllowAnonymous]
        public async Task<IActionResult> Details(int id, int pageNumber = 1)
        {
            PlaylistDTO playlistDTO = await _playlistService.GetPlaylistById(id);
            PlaylistViewModel playlistViewModel = playlistDTO.ToViewModel();

            playlistViewModel.SongsPaged = playlistViewModel.Songs.ToPagedList(pageNumber, 13);

            ViewBag.ShowEdit = _userManager.GetUserId(User) == playlistViewModel.UserId || User.IsInRole("Admin");

            return View(playlistViewModel);
        }

        public async Task<IActionResult> MyPlaylists()
        {

            IEnumerable<PlaylistDTO> playlistsDTOs = await _playlistService.GetPlaylistsByUser(_userManager.GetUserId(User));
            IList<PlaylistViewModel> playlistsViewModels = playlistsDTOs.Select(x => x.ToViewModel()).ToList();

            return View(playlistsViewModels);
        }



        [HttpPost]
        public async Task<bool> DeletePlaylist(int id)
        {
            await _playlistService.Delete(id);

            return true;
        }

        [HttpPost]
        public async Task<IActionResult> UpdateAccess(int playlistId)
        {
            await _playlistService.UpdatePublicAccess(playlistId);

            var playlist = await _playlistService.GetPlaylistById(playlistId);

            return View("Edit", playlist.ToViewModel());
        }



    }
}
