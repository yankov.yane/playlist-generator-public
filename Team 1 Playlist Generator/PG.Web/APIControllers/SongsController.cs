﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PG.Services.Contract;
using PG.Services.Mappers;
using PG.Web.Models;
using PG.Web.Models.Mappers;
using System.Linq;
using System.Threading.Tasks;

namespace PG.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Identity.Application," + JwtBearerDefaults.AuthenticationScheme)]
    public class SongsController : ControllerBase
    {
        private readonly ISongService _songService;

        public SongsController(ISongService songService)
        {
            _songService = songService;
        }

        [HttpGet]
        public async Task<IActionResult> GetSongs()
        {
            var songs = await _songService.GetAllSongs();
            var songsViewModels = songs.Select(x => x.ToViewModel());

            return Ok(songsViewModels);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetSongDetails(int id)
        {
            var song = await _songService.GetSongById(id);
            var songViewModels = song.ToViewModel();

            return Ok(songViewModels);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> CreateSong(SongViewModel model)
        {
            var song = await _songService.Create(model.ToDTO());
            var songViewModel = song.ToViewModel();

            return Created("post", songViewModel);
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateSong(int id, [FromBody] SongViewModel model)
        {
            var song = await _songService.Update(id, model.ToDTO());
            var songViewModel = song.ToViewModel();

            return Ok(songViewModel);
        }

        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteSong(int id)
        {
            await _songService.Delete(id);

            return Ok();
        }
    }
}