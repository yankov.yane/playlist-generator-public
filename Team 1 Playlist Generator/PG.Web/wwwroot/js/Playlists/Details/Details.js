﻿

$(document.body).on('click', '.playlist-pagination-numbers', function () {
    var userId = $(this).attr('data-id')
    var newPageNumber = this.innerHTML;
    $.get('/Playlists/Details', { id: userId, pageNumber: newPageNumber }, function (response) {
        $('.playlist-details').html($(response).find('.playlist-details').html());
    })
})