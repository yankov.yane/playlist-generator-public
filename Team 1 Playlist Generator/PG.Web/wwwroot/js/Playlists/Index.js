﻿
$(document).ready(function () {
    $('.filters-range').attr('max', parseInt($('.filters-range').val()) + 350)
    var range_value = $('.filters-range').val();
    var time = new Date(range_value * 1000).toISOString().substr(11, 8)
    $('.range-to').text("To: " + time);

});

$(document.body).on('input', '.filters-range', function () {
    var range_value = $(this).val();
    var time = new Date(range_value * 1000).toISOString().substr(11, 8)
    $('.range-to').text("To: " + time);
    Search();

}).on('input', '.filters-input', function () {
    Search();

}).on('input', '.filters-dropdown', function () {
    Search();

}).on('click', '.playlist-delete', function () {
    var id = $(".playlist-title").attr("data-playlist-id");

    $.post('/Playlists/DeletePlaylist', { id: id }, function (isDeleted) {
        if (isDeleted) {
            window.location.href = "/Playlists/MyPlaylists";
        }

    })

});

function Search() {
    var query = $(".filters-input").val();
    var dropdown = $(".filters-dropdown").val();
    var range = $(".filters-range").val();

    $.get('/Playlists', { searchQuery: query, genre: dropdown, duration: range }, function (response) {

        $('.playlists').html($(response).find('.playlists').html());
    })
}

