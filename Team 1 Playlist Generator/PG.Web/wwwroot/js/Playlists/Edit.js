﻿
$(document.body).on('click', '.playlist-save', function () {
    var id = $(".playlist-title").attr("data-playlist-id");
    var title = $(".playlist-title").val();
    var image = $(".playlist-pixabay-image").val();

    $.post('/Playlists/Edit', { playlistId: id, playlistTitle: title, playlistPixabayImage: image }, function (response) {
        $('.edit-playlist-form').html($(response).find('.edit-playlist-form').html());
    })

}).on('click', '.playlist-delete', function () {
    var id = $(".playlist-title").attr("data-playlist-id");

    $.post('/Playlists/DeletePlaylist', { id: id }, function (isDeleted) {
        if (isDeleted) {
            window.location.href = "/Playlists/MyPlaylists";
        }

    })

}).on('click', '.playlist-update-access', function () {
    var id = $(".playlist-title").attr("data-playlist-id");

    $.post('/Playlists/UpdateAccess', { playlistId: id }, function (response) {
        $('.edit-playlist-form').html($(response).find('.edit-playlist-form').html());

    })

});
