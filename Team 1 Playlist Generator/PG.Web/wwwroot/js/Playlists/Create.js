﻿

$(document.body).on('click', '.playlist-create-submit', function () {

    var from = $(".playlist-create-from").val();
    var to = $(".playlist-create-to").val();
    var title = $(".playlist-create-title").val();
    var metal = $(".playlist-create-genre-metal").val();
    var rock = $(".playlist-create-genre-rock").val();
    var pop = $(".playlist-create-genre-pop").val();
    var popfolk = $(".playlist-create-genre-popfolk").val();
    var topTracks = $(".playlist-create-check-toptracks").is(":checked");
    var sameArtist = $(".playlist-create-check-sameartist").is(":checked");

    $.post('/Playlists/Create', {
        startLocation: from,
        endLocation: to,
        playlistName: title,
        metal: metal,
        rock: rock,
        pop: pop,
        chalga: popfolk,
        topTracks: topTracks,
        sameArtist: sameArtist,
    }, function (response) {
        if (response > 0) {
            window.location.href = "/Playlists/Details/" + response;
        }
    })

}).on('input', '.genre-range', function () {

    var value = $(this).val();
    var sibling = $(this).next();

    sibling.text(value + '%');
})