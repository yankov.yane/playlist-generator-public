﻿
$(document.body).on('click', '.button-ban-user', function () {
    var button = this;
    var userId = $(this).attr('data-user-id')
    $.post('/Admin/BanUsers', { userId: userId }, function (isBanned) {
        if (isBanned) {
            $(button).removeClass().toggleClass('btn btn-outline-secondary button-unban-user');
            $(button).text("Unban user");
        }
    })

}).on('click', '.button-unban-user', function () {
    var button = this;
    var userId = $(this).attr('data-user-id')
    $.post('/Admin/UnbanUsers', { userId: userId }, function (isUnbanned) {
        if (isUnbanned) {
            $(button).removeClass().toggleClass('btn btn-outline-danger button-ban-user');
            $(button).text("Ban user");
        }
    })

}).on('click', '.button-delete-user', function () {
    var userId = $(this).attr('data-user-id')
    $.post('/Admin/DeleteUser', { userId: userId }, function (response) {
        $('#usersInfo').html($(response).find('#usersInfo'));
    })

}).on('click', '.pagination-numbers', function () {
    var newPageNumber = this.innerHTML;
    $.get('/Admin/Users', { pageNumber: newPageNumber }, function (response) {
        $('#usersInfo').html($(response).find('#usersInfo'));
    })

});
